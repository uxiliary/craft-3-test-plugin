<?php
/**
 * Craft 3 Test plugin for Craft CMS 3.x
 *
 * a test plugin
 *
 * @link      https://uxiliary.com/
 * @copyright Copyright (c) 2018 Uxiliary
 */

namespace uxiliarycraft3test\craft3test\services;

use uxiliarycraft3test\craft3test\Craft3Test;

use Craft;
use craft\base\Component;

/**
 * Craft3TestService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Uxiliary
 * @package   Craft3Test
 * @since     1.0.0
 */
class Craft3TestService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Craft3Test::$plugin->craft3TestService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
