<?php
/**
 * Craft 3 Test plugin for Craft CMS 3.x
 *
 * a test plugin
 *
 * @link      https://uxiliary.com/
 * @copyright Copyright (c) 2018 Uxiliary
 */

/**
 * Craft 3 Test en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('craft-3-test', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Uxiliary
 * @package   Craft3Test
 * @since     1.0.0
 */
return [
    'Craft 3 Test plugin loaded' => 'Craft 3 Test plugin loaded',
];
