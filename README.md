# Craft 3 Test plugin for Craft CMS 3.x

a test plugin

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /craft-3-test

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Craft 3 Test.

## Craft 3 Test Overview

-Insert text here-

## Configuring Craft 3 Test

-Insert text here-

## Using Craft 3 Test

-Insert text here-

## Craft 3 Test Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Uxiliary](https://uxiliary.com/)
